Introduction
============

This repository contains iPython notebook implementations of all of the code, data anaylsis and visualizations
appearing in the text:

Janert (2010).  "Data Analysis with Open Source Tools"

http://www.amazon.com/Data-Analysis-Open-Source-Tools/dp/0596802358/ref=sr_1_1?ie=UTF8&qid=1400874181&sr=8-1&keywords=data+analysis+with+open+source+tools

The iPython notebboks show example implementations using the Python language.  I make heavy use of NumPy,
matplotlib and the Pandas libraries for the visualization and analysis of the presented data.  You need to
have these libraries installed in order to use the notebooks fully.

The datasets used in these notebooks were obtained from the O'Reilly
website page for the book:

http://examples.oreilly.com/9780596802363/

The glass identification data set is fromm the UCI Machine Learning
repository:

http://archive.ics.uci.edu/ml/datasets/Glass+Identification

The monthly CO_2 concentration data

http://scrippsco2.ucsd.edu/data/in_situ_co2/monthly_mlo.csv

The 40 year Nikkei Stock Index data

http://indexes.nikkei.co.jp/en/nkave/archives/data
